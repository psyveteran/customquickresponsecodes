### What is this repository for?? ###

* Repo holds a Java / JavaFX application that creates QR codes for large public keys.
* Why? I intend to re-use this codebase for a retro and private password manager product on Android.

### Versioning ###
versioning in English.

Current: PREDEV
Next: INDEV

### How do I get set up? ###

<coming when ALPHA>

### Who may I talk to? ###

* Contact me via fosterlamb.studio/
* Reach out to me via @psyveteran on Twitter.